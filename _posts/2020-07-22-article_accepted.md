---
title: "New article published!"
layout: post
shortnews: true
icon: newspaper-o
---

New article published! [link](https://ieeexplore.ieee.org/abstract/document/9139484)
