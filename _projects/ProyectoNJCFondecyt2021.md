---
title: Routing, space and spectrum allocation methods for SDM flexgrid optical networks
subtitle: ANID FONDECYT Iniciación 11201024

description: |
  The proyect pretens to solve the essential problems for designing future optical communication networks (EON and SDM architectures) facing the impending capacity crunch on telecommunication networks.
  
people:
  - nicolasJara	
  - patriciaMorales
  - jorgeBermudez

layout: project
last-updated: 2020-05-15
---

The rapid increase in bandwidth demand from existing networks has caused a growth in the use of telecommunications technologies, especially WDM optical networks, since they can transmit vast amounts of data. So far, communication technologies have been able to meet bandwidth demands. Nevertheless, this decade researchers have anticipated a potential Capacity Crunch associated with these networks. It refers to the fact that the transmission capacity limit in optical fibers is close to being reached soon. It is then urgent to make the current network architectures evolve to satisfy the relentless growth in bandwidth demands. 

Two proposals have been mainly addressed so far to face the capacity crunch. Elastic Optical Networks (EON) is a new network architecture paradigm allowing to flexibly allocate the frequency spectrum available on each network link to attend different types of traffic adaptively. This elasticity provides new characteristics to the network, where the spectrum usage can be efficiently managed, improving the network resources. To solve the second one is to migrate current networks from a static operation to a dynamic operation (this is, allocating the resources to each user only when a transmission is requested). This technology can be already deployed, but it has not been implemented since economic savings are not enough to convince communication enterprises to incorporate this operation scheme in current networks.

Also, on a resource multiplication point of view, SDM appears as a promising technology to complement both dynamic and elastic optical networks. By enhancing the spatial diversity with multicore fibers and consequently providing more capacity and network throughput than simple EON, SDM can compensate for the forthcoming capacity crunch when combined with WDM/EON architectures.

The design of optical networks decomposes into many different tasks, where engineers must organize the way the primary system's resources are used, minimizing the design and operation costs and respecting critical performance constraints. To consider all these tasks is out of the scope of this FONDECYT project. Nevertheless, I am going to focus on some of the basics and fundamental problems on these networks. These are Routing and Space and Spectrum assignment (RSSA) problem in SDM-EON architectures; Routing, Modulation format and Spectrum assignment (RMLSA) problems in wide-area networks considering physical impairment since they cannot be entirely compensated; the regenerator placement and allocation problems in wide-area EON and SDM networks; and finally the integration of machine learning techniques to apply reinforcement learning on the network operation.  Each one of them has the goal of minimizing the overall network capacity while guaranteeing a minimum level of quality of service to the user (measured as blocking probability).

The leading researcher has accumulated some experience in analyzing and designing communication systems, and in performing this analysis from different points of view. The ideas for designing optical infrastructures facing the impending Capacity Crunch problem come from previous projects. However, we have not addressed this issue yet because it started to be being an issue, and a critical one, rather recently. Based on this experience, we propose hypotheses which we believe may improve the efficiency of the network resource management. These are: applying new order policies to avoid spectrum fragmentation; achieving capacity savings by balancing the user's traffic load, and bandwidth demands; obtaining a joint solution to the RSSA and RMLSA problems based on analytical solutions; considering linear and non-linear physical impairments on the RMLSA problem in wide-area networks; deploying regenerators to increase optical reach and decrease the blocking probability of the network, while minimizing CAPEX and OPEX; and integrating machine learning techniques to improve optical network performance.  

The recently described problems have a significant economic, technique, and scientific importance since any improvement may change the optical network infrastructure. This situation opens an opportunity to provide a meaningful contribution on said networks, enabling to face the threat of a capacity crunch in optical networks. In practical terms, this allows to highly increase the network capacity, in the sense of traffic demands, but with a meager investment.

### people

- Nicolás Jara (Departamento de Electrónica, USM).
- Patricia Morales (Estudiantes doctorado, Departamento de Electrónica, USM)
- Jorge Bermudez (Estudiantes Magister, Departamento de Electrónica, USM)