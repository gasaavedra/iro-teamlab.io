---
title: Comunicaciones ópticas para el internet del futuro
subtitle: Proyecto USM PI_LII_2020_74

description: |
  Este proyecto se enfoca en el desarrollo de nuevas estrategias para mejorar el manejo de recursos en arquitecturas de red óptica elástica (EON), y arquitecturas con multiplexación espacial (SDM). 

people:
  - nicolasJara
  - ricardoOlivares
  - astridLozada
  - patriciaMorales
  - jorgeBermudez

layout: project
last-updated: 2020-07-23
---

La aparición de numerosas aplicaciones basadas en Internet requiere transmitir cada vez más un mayor volumen de información. Esto ha causado un incremento considerable de demanda de ancho de banda a la infraestructura de telecomunicaciones, en particular las tecnologías basadas en las redes ópticas. Hasta el momento, las tecnologías de comunicación han podido cumplir con las demandas de ancho de banda. Sin embargo, en la última década investigadores han reconocido la amenaza de un colapso de capacidad (en inglés “Capacity Crunch”) en las redes de telecomunicaciones, el cual se traduce en el hecho de que estamos próximos a alcanzar la capacidad límite de transmisión de la fibra óptica. Este colapso afecta todas las redes de comunicación, tanto inalámbricas como cableadas. Esta situación significa que las arquitecturas de redes de telecomunicaciones deben evolucionar con tal de poder satisfacer este incesante crecimiento de demanda de ancho de banda, para evitar consecuencias negativas en sus usuarios. Observe que debido a que las redes ópticas son la base de nuestros sistemas de comunicación a nivel mundial, cualquier progreso en su rendimiento conlleva considerables mejores sociales y económicas. Por la misma razón, cualquier amenaza de seguridad y cualquier obstáculo en el flujo de su tráfico lleva a pérdidas de productividad y posible estancamiento económico. Esta es la razón por la cual el diseño de redes ópticas es actualmente un tópico que concentra un importante esfuerzo en investigación y desarrollo.

Hace algunos años existen algunas propuestas tecnológicas para resolver el capacity crunch en el contexto de redes ópticas. A continuación, se listan las más importantes:

-	Redes ópticas elásticas (su acrónimo en ingles EON). Este nuevo paradigma de arquitectura permite usar el espectro de frecuencia de forma flexible, así atendiendo las necesidades de tráfico adaptivamente, entregando solamente el ancho de banda necesario para cada usuario (ni más, ni menos). Para lograr esto, primero, el espectro de frecuencia se divide en varias unidades de slot de frecuencia (llamados FSU), y, luego, los FSU se agrupan para satisfacer la demanda de ancho de banda de cada transmisión. Esta arquitectura ha sido investigada en los últimos años, y aún requiere un importante desarrollo e investigación para ser implementado en la práctica.

-	Multiplexación por división espacial (su acrónimo en ingles SDM). Este enfoque, utilizando tanto en redes inalámbricas como de fibra, propone dividir los canales de comunicación físicamente. En redes ópticas esto significa modificar los cables de fibra óptica, los cuales contarían con más de un núcleo (fibras multinúcleo), evitando así instalar múltiples cables de fibra óptica. Esta actualización de la fibra óptica está en desarrollo, y requiere un mayor esfuerzo en investigación para poder estar disponible en un futuro cercano.

El diseño de redes ópticas es un tema muy actual tanto a nivel industrial como académico. El análisis previo ha incentivado a investigadores a proveer nuevas estrategias y líneas de desarrollo en estas nuevas tecnologías, en el cual nuestro proyecto se enfoca completamente. El grupo de investigadores que componen esta propuesta han acumulado una larga experiencia no solo en el diseño y análisis de redes ópticas, si no en el desarrollo de técnicas para medir los efectos físicos de las comunicaciones ópticas, y la resolución de problemas en base a técnicas de optimización, desarrollando metodologías para desarrollar estas tareas. En este proyecto, aplicaremos nuestra habilidad para atacar varios problemas asociados al manejo eficiente de los siempre limitados recursos de la red, considerando los impedimentos físicos que tiene la comunicación de fibra óptica, especialmente en redes de larga envergadura. A continuación, se listan los problemas que se desean resolver en el contexto de este proyecto.

-	Enrutamiento, asignación de formato de modulación y espectro de frecuencia (en inglés RMLSA): Uno de los problemas más importantes a resolver por los operadores de red óptica consiste en resolver la asignación de rutas y una porción de espectro de frecuencia para cada solicitud de conexión. Este problema se le conoce como RSA. Sin embargo, en redes de larga envergadura, como las continentales o incluso mayores, este problema se vuelve aún más complejo, debido a que los operadores deben considerar la relación entre el formato de modulación y el máximo alcance (en kilómetros) de cada solicitud de transmisión. Este problema se le conoce RMLSA. Entonces, para una solicitud de comunicación dada, el problema de RMLSA consiste en encontrar un camino, un formato de modulación apropiado para el largo de dicho camino, y la asignación de una porción del espectro óptica en dicha ruta. Este problema es crucial para el diseño de redes por lo que deseamos resolverlo con la experiencia adquirida previamente.

-	Políticas de asignación de recursos: En el proceso de asignación de recursos definido en el RMLSA, se espera definir procedimientos de gestión de recursos para control el montaje de las conexiones con algún tipo de servicio, con tal de mejorar el rendimiento total de la red. El objetivo es utilizar información como la utilización de la red y el nivel de fragmentación del espectro para decidir si una conexión puede ser establecida, posterior a la confirmación de que la disponibilidad de recursos para su transmisión.

-	Consideración de los efectos físicos: La comunicación óptica es solo realizable si se logra un nivel de calidad de transmisión (QoT) aceptable al transmitir de fuente a destino, considerando la acumulación de impedimentos de la capa física (en inglés, Physical Layer Impairments - PLI), lo que degrada las señales transmitidas. En los enlaces de fibra, el ruido de emisión espontánea amplificado (ASE) y la distorsión no lineal son acumulativos, y en la práctica, no se pueden compensar completamente. Las soluciones de RMLSA deben asegurar tanto una calidad de servicio mínima, y una calidad de transmisión mínima a cada solicitud de conexión, considerando los PLI presentes en la fibra óptica. Por lo tanto, se requiere modelar los problemas físicos apropiadamente, y posteriormente considerarlos en el proceso de diseño de estas redes. Inclusive, en redes de larga envergadura existe la posibilidad de no poder llegar a destino a través de una comunicación transparente (100% óptica), por lo que la instalación de regeneradores 3R es una necesidad primordial, ya que reciben la señal degradada y la recrean nuevamente en el dominio electrónico. Estos dispositivos permiten transmitir a mayores distancias, sin embargo, la comunicación deja de ser transparente, creando retardos importantes en su propagación, además de agregar un costo importante a la red, por lo que optimizar la ubicación y asignación de estos dispositivos en la red es de suma importancia.

### Participantes

- Nicolás Jara (Departamento de Electrónica, USM).
- Ricardo Olivares (Departamento de Electrónica, USM)
- Victor Albornoz (Departamento de Industrias, USM).
- Astrid Lozada (Estudiante de doctorado, Departamento de Electrónica, USM)
- Patricia Morales (Estudiante de doctorado, Departamento de Electrónica, USM)
- Jorge Bermudez (Estudiante de Magister, Departamento de Electrónica, USM)
- Claudio Gonzalez (Estudiante de Magister, Departamento de Indústrias, USM)